package com.example.soicaumienbac;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Interface {

    String JSONURL = "https://gitlab.com/snippets/1894997/";

    @GET("raw")
    Call<String> getString();
}
