package com.example.soicaumienbac.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.soicaumienbac.Interface;
import com.example.soicaumienbac.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private String text;
    TextView textView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        textView = root.findViewById(R.id.text_dashboard);
        getJSONResponse();

        return root;
    }

    private void getJSONResponse(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Interface.JSONURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        Interface api = retrofit.create(Interface.class);

        Call<String> call = api.getString();

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        String jsonresponse = response.body();
                        try{
                        JSONObject obj = new JSONObject(jsonresponse);
                        if(obj.optString("status").equals("ok")) {
                            JSONArray dataArray = obj.getJSONArray("cau_dac_biet");
                            String number = dataArray.toString();
                            String number2 = number.replace("\"","").replace("[","").replace("]","");
                            String[] numbers = number2.split(",",2);
                            for(String number4:numbers){
                                text = (number4 + "\n");
                            }

                            JSONArray dataArray2 = obj.getJSONArray("lotto_choi_nhieu");
                            String number22 = dataArray2.toString();
                            String number222 = number22.replace("\"","").replace("[","").replace("]","");
                            String[] numbers2 = number222.split(",",2);
                            for(String number4:numbers2){
                                text += (number4 + "\n");
                            }


                            textView.setText(text);
                        }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

}